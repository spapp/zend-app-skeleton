<?php
class ErrorController extends Zend_Controller_Action {
    /**
     * Error action
     *
     * @return void
     */
    public function errorAction() {
        $applicationEnv = $this->getFrontController()->getParam('bootstrap')->getApplication()->getEnvironment();

        if ($applicationEnv !== 'development') {
            // FIXME loging errors
            $this->redirect('/');
        } else {
            $this->handleError();
        }
    }

    /**
     * Handle error
     *
     * @return void
     */
    protected function handleError() {
        $errors = $this->_getParam('error_handler');

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Application error';
                break;
        }

        $this->view->exception = $errors->exception;
        $this->view->request = $errors->request;
    }
}
