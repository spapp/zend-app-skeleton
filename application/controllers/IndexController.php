<?php
class IndexController extends Zend_Controller_Action {
    /**
     * Controller initialization.
     *
     * @return void
     */
    public function init() {
        $this->_helper->log(1,1);
        $this->_helper->log('2',2);
        $this->_helper->log('info');
        $this->_helper->log(array('info'));
    }

    /**
     * Index (default) action.
     *
     * @return void
     */
    public function indexAction() {}
}
