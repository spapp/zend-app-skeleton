<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.21.
 */
/**
 * Class Application_View_Filter_HtmlMinify
 */
class Application_View_Filter_HtmlMinify implements Zend_Filter_Interface {
    /**
     * Returns the result of filtering $value
     *
     * @param  string $value
     *
     * @return string
     */
    public function filter($value) {
        // TODO pre, script, style tag ... ?
        $value = preg_replace(
            array(
                '~>\s+<~',
                '~' . PHP_EOL . '\s+<~'
                /*,
                                '~(\s)+~s'*/
            ),
            array(
                '><',
                PHP_EOL . '<'
                /*,
                                '\\1'*/
            ),
            $value
        );

        return $value;
    }
} 