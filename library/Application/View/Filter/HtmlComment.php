<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.27.
 */
/**
 * Class Application_View_Filter_HtmlComment
 */
class Application_View_Filter_HtmlComment implements Zend_Filter_Interface {
    /**
     * Returns the result of filtering $value
     *
     * Remove HTML comments.
     * Not remove IE conditional comments if begin like "<!--[".
     *
     * @param  string $value
     *
     * @return string
     */
    public function filter($value) {
        return preg_replace('~<!--[^\[]([\s\S]*?)-->~', '', $value);
    }
} 