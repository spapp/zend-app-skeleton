<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.25.
 */
/**
 * Class Application_Skin_Lessc
 */
class Application_Skin_Lessc extends Application_Skin_Abstract {
    /**
     * Less file suffix.
     */
    const LESS_FILE_SUFFIX = '.less';

    /**
     * lessc command.
     *
     * @var string
     */
    protected $lessc = 'lessc';

    /**
     * Lessc arguments.
     *
     * Default args:
     *      -s  Suppress output of error messages.
     *      -x  Compress output by removing some whitespaces.
     *
     * @var array
     */
    protected $args = array(
        '-x',
        '-s'
    );

    /**
     * Sets path to lessc command.
     *
     * @param string $lessc
     *
     * @return $this
     */
    public function setLessc($lessc) {
        $this->lessc = $lessc;

        return $this;
    }

    /**
     * Returns lessc command.
     *
     * @return string
     */
    public function getLessc() {
        return $this->lessc;
    }

    /**
     * If it sets TRUE then compress output.
     *
     * @param bool $compress
     *
     * @return $this
     */
    public function setCompress($compress) {
        $this->addArg('x', $compress);

        return $this;
    }

    /**
     * If it sets TRUE then suppress output of error messages.
     *
     * @param bool $silent
     *
     * @return $this
     */
    public function setSilent($silent) {
        $this->addArg('s', $silent);

        return $this;
    }


    /**
     * Returns raw style.
     *
     * @return string
     */
    public function getRawStyle() {
        return shell_exec($this->getCmd());
    }

    /**
     * Sets lessc arguments.
     *
     * @param array $args
     *
     * @return $this
     */
    public function setArgs(array $args) {
        foreach ($args as $name => $value) {
            $this->addArg($name, $value);
        }

        return $this;
    }

    /**
     * Adds a lessc argument.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function addArg($name, $value) {
        $name = (strlen($name) > 1) ? ('--' . $name) : ('-' . $name);

        switch (true) {
            case (true === $value):
                if (false === in_array($name, $this->args)) {
                    array_push($this->args, $name);
                }
                break;
            case (false === $value):
                if (true === in_array($name, $this->args)) {
                    unset($this->args[array_search($name, $this->args)]);
                }
                break;
            default:
                array_push($this->args, $name . '=' . $value);
        }

        return $this;
    }

    /**
     * Returns lessc arguments.
     *
     * @return array
     */
    public function getArgs() {
        return $this->args;
    }

    /**
     * Returns cli string.
     *
     * @return string
     */
    protected function getCmd() {
        $cmd = array_merge(array($this->getLessc()), $this->getArgs());

        array_push($cmd, realpath($this->getPath()) . '/' . $this->getSkinName() . self::LESS_FILE_SUFFIX);

//        if (false === $this->isSilent()) {
//            array_push($cmd, '2>&1');
//        }

        return implode(' ', $cmd);
    }
} 