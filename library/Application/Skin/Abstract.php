<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.22.
 */
/**
 * Class Application_Skin_Abstract
 */
abstract class Application_Skin_Abstract {
    /**
     * Skin content type.
     *
     * @var string
     */
    protected $contentType = 'text/css';

    /**
     * Skin file character set.
     *
     * @var string
     */
    protected $charset = 'utf-8';

    /**
     * Controller name.
     *
     * @var string
     */
    protected $controllerName = 'skin';

    /**
     * Skin name.
     *
     * @var string
     */
    protected $skinName = null;

    /**
     * @var string
     */
    protected $defaultSkin = 'default';

    /**
     * Single skin files.
     *
     * @var array
     */
    protected $files = array();

    /**
     * Path to skin.
     *
     * @var string
     */
    protected $path = '';

    /**
     * @var bool
     */
    protected $cache = false;

    /**
     * @var string
     */
    protected $cookieId = 'skin';
    /**
     * @var string
     */
    protected $comment = '';

    /**
     * Constructor
     */
    public function __construct($options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        } else if ($options instanceof Zend_Config) {
            $this->setOptions($options->toArray());
        }
    }

    /**
     * Set options from array
     *
     * @param  array $options Configuration for skin
     *
     * @return $this
     */
    public function setOptions(array $options) {
        foreach ($options as $key => $value) {
            $method = 'set' . strtolower($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }
    /**
     * Sets comment from file.
     *
     * @param string $file
     *
     * @return $this
     */
    public function setFileComment($file) {
        if (file_exists($file)) {
            $this->comment = file_get_contents($file);
        }

        return $this;
    }

    /**
     * Sets comment from string.
     *
     * @param string $comment
     *
     * @return $this
     */
    public function setComment($comment) {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Returns global file comment.
     *
     * @return string
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * Sets skin content type.
     *
     * @param string $contentType only mime type (without charset)
     *
     * @return $this
     */
    public function setContentType($contentType) {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Returns skin content type http header string.
     *
     * @return string
     */
    public function getContentType() {
        $contentType = $this->contentType;

        if ($this->getCharset()) {
            $contentType .= ';charset=' . $this->getCharset();
        }

        return $contentType;
    }

    /**
     * Returns all http headers.
     *
     * @return array
     */
    public function getHeaders() {
        $headers = array(
            'Content-Type' => $this->getContentType()
        );

        return $headers;
    }

    /**
     * Sets skin cookie name.
     *
     * @param string $cookieId
     *
     * @return $this
     */
    public function setCookieId($cookieId) {
        $this->cookieId = $cookieId;

        return $this;
    }

    /**
     * Returns skin cookie name.
     *
     * @return string
     */
    public function getCookieId() {
        return $this->cookieId;
    }

    /**
     * Sets default skin name.
     *
     * @param string $skinName
     *
     * @return $this
     */
    public function setDefaultSkin($skinName) {
        $this->defaultSkin = $skinName;

        return $this;
    }

    /**
     * Returns default skin name.
     *
     * @return string
     */
    public function getDefaultSkin() {
        return $this->defaultSkin;
    }

    /**
     * Sets current skin name.
     *
     * @param string $skinName
     *
     * @return $this
     */
    public function setSkinName($skinName) {
        if (trim($skinName)) {
            $this->skinName = $skinName;
        }

        return $this;
    }

    /**
     * Returns current skin name.
     *
     * If skin name was set then returns it.
     * If it do not set then reads from cookie otherwise returns the default skin name.
     *
     * @return string
     */
    public function getSkinName() {
        if (null !== $this->skinName) {
            return $this->skinName;
        }

        return Zend_Controller_Front::getInstance()->getRequest()->getCookie(
                                    $this->getCookieId(),
                                    $this->getDefaultSkin()
        );
    }

    /**
     * Sets controller name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setControllerName($name) {
        $this->controllerName = $name;

        return $this;
    }

    /**
     * Returns controller name.
     *
     * @return string
     */
    public function getControllerName() {
        return $this->controllerName;
    }

    /**
     * Sets the single style files.
     *
     * @param array $files
     *
     * @return $this
     */
    public function setFiles(array $files) {
        $this->files = $files;

        return $this;
    }

    /**
     * Returns the single style files.
     *
     * @return array
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * Sets style file character set.
     *
     * @param string $charset
     *
     * @return $this
     */
    public function setCharset($charset) {
        $this->charset = $charset;

        return $this;
    }

    /**
     * Returns style file character set.
     *
     * @return string
     */
    public function getCharset() {
        return $this->charset;
    }

    /**
     * Sets skins folder name.
     *
     * @param string $path
     *
     * @return $this
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Returns skins folder name.
     *
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Sets cache object options.
     *
     * If it is FALSE then disable caching.
     *
     * @param string|bool|array $cache
     *
     * @return $this
     */
    public function setCache($cache) {
        $this->cache = $cache;

        return $this;
    }

    /**
     * Returns cache object.
     *
     * @return array|bool|string
     */
    protected function getCache() {
        if (is_string($this->cache)) {
            $cacheManager = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('cacheManager');

            if (true === $cacheManager->hasCache($this->cache)) {
                $this->cache = $cacheManager->getCache($this->cache);
            }
        } elseif (is_array($this->cache)) {
            $this->cache = Zend_Cache::factory(
                                     $this->cache['frontend']['name'],
                                     $this->cache['backend']['name'],
                                     isset($this->cache['frontend']['options']) ? $this->cache['frontend']['options'] : array(),
                                     isset($this->cache['backend']['options']) ? $this->cache['backend']['options'] : array(),
                                     isset($this->cache['frontend']['customFrontendNaming']) ? $this->cache['frontend']['customFrontendNaming'] : false,
                                     isset($this->cache['backend']['customBackendNaming']) ? $this->cache['backend']['customBackendNaming'] : false,
                                     isset($this->cache['frontendBackendAutoload']) ? $this->cache['frontendBackendAutoload'] : false
            );
        }

        return $this->cache;
    }

    /**
     * Returns TRUE then cache object was set.
     *
     * @return bool
     */
    public function hasCache() {
        return (bool)$this->getCache();
    }

    /**
     * Magice function.
     *
     * @return string
     */
    public function __toString() {
        if (true === $this->hasCache()) {
            if (false === ($data = $this->getCache()->load(md5($this->getLinkHref())))) {
                $data = $this->getComment() . PHP_EOL .$this->getRawStyle();
                $this->getCache()->save($data);
            }
        } else {
            $data = $this->getComment() . PHP_EOL . $this->getRawStyle();
        }

        return (string)$data;
    }

    /**
     * Returns style href to html link tag.
     *
     * @return string
     */
    public function getLinkHref() {
        $link = array(
            '',
            $this->getControllerName()
        );

        if ($this->getSkinName() !== $this->getDefaultSkin()) {
            array_push($link, $this->getSkinName());
        }

        return implode(DIRECTORY_SEPARATOR, $link);
    }

    /**
     * Returns raw style.
     *
     * @return mixed
     */
    abstract public function getRawStyle();
} 