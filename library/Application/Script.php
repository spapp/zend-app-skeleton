<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.26.
 */
/**
 * Class Application_Script
 */
class Application_Script {
    /**
     * Constructor
     */
    protected function __construct() {

    }

    /**
     * Returns a script adapter.
     *
     * @param string                 $adapterName
     * @param null|array|Zend_Config $options
     *
     * @return Application_Script_Abstract
     */
    public static function factory($adapterName, $options = null) {
        $className = 'Application_Script_' . ucfirst(strtolower($adapterName));

        return new $className($options);
    }
} 