<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.26.
 */
/**
 * Class Application_Script_Abstract
 */
abstract class Application_Script_Abstract {
    /**
     * Skin content type.
     *
     * @var string
     */
    protected $contentType = 'application/javascript';

    /**
     * Skin file character set.
     *
     * @var string
     */
    protected $charset = 'utf-8';

    /**
     * Controller name.
     *
     * @var string
     */
    protected $controllerName = 'script';

    /**
     * Single skin files.
     *
     * @var array
     */
    protected $files = array();

    /**
     * Path to skin.
     *
     * @var string
     */
    protected $path = '';

    /**
     * @var bool
     */
    protected $cache = false;

    /**
     * @var bool
     */
    protected $compress = true;

    /**
     * @var bool
     */
    protected $minify = true;

    /**
     * @var string
     */
    protected $comment = '';

    /**
     * @var string|null
     */
    protected $scriptName = null;

    /**
     * Constructor
     */
    public function __construct($options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        } else if ($options instanceof Zend_Config) {
            $this->setOptions($options->toArray());
        }
    }

    /**
     * Set options from array
     *
     * @param  array $options Configuration for skin
     *
     * @return $this
     */
    public function setOptions(array $options) {
        foreach ($options as $key => $value) {
            $method = 'set' . strtolower($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    /**
     * Sets files content type.
     *
     * @param string $contentType only mime type (without charset)
     *
     * @return $this
     */
    public function setContentType($contentType) {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Returns files content type http header string.
     *
     * @return string
     */
    public function getContentType() {
        $contentType = $this->contentType;

        if ($this->getCharset()) {
            $contentType .= ';charset=' . $this->getCharset();
        }

        return $contentType;
    }

    /**
     * Returns all http headers.
     *
     * @return array
     */
    public function getHeaders() {
        $headers = array(
            'Content-Type' => $this->getContentType()
        );

        return $headers;
    }

    /**
     * Sets controller name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setControllerName($name) {
        $this->controllerName = $name;

        return $this;
    }

    /**
     * Returns controller name.
     *
     * @return string
     */
    public function getControllerName() {
        return $this->controllerName;
    }

    /**
     * Sets the files.
     *
     * @param string|array $files
     *
     * @return $this
     */
    public function setFiles($files) {
        $this->files = $files;

        return $this;
    }

    /**
     * Returns the script files.
     *
     * @return array
     */
    public function getFiles() {
        if (is_string($this->files) and file_exists($this->getBasePath() . DIRECTORY_SEPARATOR . $this->files)) {
            $this->files = file($this->getBasePath() . DIRECTORY_SEPARATOR . $this->files);
        }

        return $this->files;
    }

    /**
     * Sets file character set.
     *
     * @param string $charset
     *
     * @return $this
     */
    public function setCharset($charset) {
        $this->charset = $charset;

        return $this;
    }

    /**
     * Returns file character set.
     *
     * @return string
     */
    public function getCharset() {
        return $this->charset;
    }

    /**
     * Sets script base folder name.
     *
     * @param string $path
     *
     * @return $this
     */
    public function setBasePath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Returns script base folder name.
     *
     * @return string
     */
    public function getBasePath() {
        return $this->path;
    }

    /**
     * Sets cache object options.
     *
     * If it is FALSE then disable caching.
     *
     * @param string|bool|array $cache
     *
     * @return $this
     */
    public function setCache($cache) {
        $this->cache = $cache;

        return $this;
    }

    /**
     * Returns cache object.
     *
     * @return array|bool|string
     */
    protected function getCache() {
        if (is_string($this->cache)) {
            $cacheManager = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('cacheManager');

            if (true === $cacheManager->hasCache($this->cache)) {
                $this->cache = $cacheManager->getCache($this->cache);
            }
        } elseif (is_array($this->cache)) {
            $this->cache = Zend_Cache::factory(
                                     $this->cache['frontend']['name'],
                                     $this->cache['backend']['name'],
                                     isset($this->cache['frontend']['options']) ? $this->cache['frontend']['options'] : array(),
                                     isset($this->cache['backend']['options']) ? $this->cache['backend']['options'] : array(),
                                     isset($this->cache['frontend']['customFrontendNaming']) ? $this->cache['frontend']['customFrontendNaming'] : false,
                                     isset($this->cache['backend']['customBackendNaming']) ? $this->cache['backend']['customBackendNaming'] : false,
                                     isset($this->cache['frontendBackendAutoload']) ? $this->cache['frontendBackendAutoload'] : false
            );
        }

        return $this->cache;
    }

    /**
     * Returns TRUE then cache object was set.
     *
     * @return bool
     */
    public function hasCache() {
        return (bool)$this->getCache();
    }

    /**
     * If it sets TRUE then compress output.
     *
     * @param bool $compress
     *
     * @return $this
     */
    public function setCompress($compress) {
        $this->compress = (bool)$compress;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCompress() {
        return $this->compress;
    }

    /**
     * Sets script name.
     *
     * @param string $scriptName
     *
     * @return $this
     */
    public function setScriptName($scriptName) {
        $this->scriptName = $scriptName;

        return $this;
    }

    /**
     * Returns script name if it is exists.
     *
     * @return null|string
     */
    public function getScriptName() {
        return $this->scriptName;
    }

    /**
     * Sets minify.
     *
     * @param bool $minify
     *
     * @return $this
     */
    public function setMinify($minify) {
        $this->minify = (bool)$minify;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMinify() {
        return $this->minify;
    }


    /**
     * Sets comment from file.
     *
     * @param string $file
     *
     * @return $this
     */
    public function setFileComment($file) {
        if (file_exists($file)) {
            $this->comment = file_get_contents($file);
        }

        return $this;
    }

    /**
     * Sets comment from string.
     *
     * @param string $comment
     *
     * @return $this
     */
    public function setComment($comment) {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Returns global file comment.
     *
     * @return string
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * Magice function.
     *
     * @return string
     */
    public function __toString() {
        if (true === $this->hasCache()) {
            if (false === ($data = $this->getCache()->load(md5($this->getScriptSrc($this->getScriptName()))))) {
                $data = $this->getComment() . PHP_EOL . $this->getRawBody();
                $this->getCache()->save($data);
            }
        } else {
            $data = $this->getComment() . PHP_EOL . $this->getRawBody();
        }

        return (string)$data;
    }

    /**
     * Returns script src.
     *
     * @param null $fileName
     *
     * @return string
     */
    public function getScriptSrc($fileName = null) {
        $src = array(
            '',
            $this->getControllerName()
        );

        if ($fileName) {
            array_push($src, trim($fileName));
        }

        return implode(DIRECTORY_SEPARATOR, $src);
    }

    /**
     * Returns src to html script tag.
     *
     * @return array
     */
    public function getScripts() {
        $links = array();

        if (true === $this->isCompress()) {
            array_push($links, $this->getScriptSrc());
        } else {
            $files = $this->getFiles();

            foreach ($files as $file) {
                if (!preg_match('~^#|^//~i', $file)) {
                    array_push($links, $this->getScriptSrc(trim($file)));
                }
            }
        }

        return $links;
    }


    /**
     * Returns raw script.
     *
     * @return string
     */
    abstract public function getRawBody();
} 