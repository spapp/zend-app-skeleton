<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.26.
 */
/**
 * Class Application_Script_Jsminc
 */
class Application_Script_Jsminc extends Application_Script_Abstract {
    /**
     * jsminc command.
     *
     * @var string
     */
    protected $jsminc = 'jsminc';

    /**
     * Sets path to jsminc command.
     *
     * @param string $jsminc
     *
     * @return $this
     */
    public function setJsminc($jsminc) {
        $this->jsminc = $jsminc;

        return $this;
    }

    /**
     * Returns jsminc command.
     *
     * @return string
     */
    public function getJsminc() {
        return $this->jsminc;
    }

    /**
     * Returns raw style.
     *
     * @return string
     */
    public function getRawBody() {
        $output = array();

        if (true === $this->isCompress()) {
            $files = $this->getFiles();

            foreach ($files as $file) {
                if (!preg_match('~^#|^//~i', $file)) {
                    $output[] = $this->getFileContents(trim($file));
                }
            }
        } else {
            $output[] = $this->getFileContents(trim($this->getScriptName()));
        }

        return implode(' ', $output);
    }

    /**
     * Returns file content.
     *
     * @param string $fileName
     *
     * @return string
     */
    protected function getFileContents($fileName) {
        $file = $this->getBasePath() . DIRECTORY_SEPARATOR . trim($fileName);
        $output = array();

        if (false === file_exists($file)) {
            if (false === $this->isMinify()) {
                $output[] = '// // File not exists: ' . $fileName;
            }
        } else if (true === $this->isMinify()) {
            $output[] = shell_exec($this->getJsminc() . ' < ' . $file);
        } else {
            $output[] = '// // ' . trim($fileName);
            $output[] = file_get_contents($file);
        }

        return implode(PHP_EOL, $output);
    }
} 