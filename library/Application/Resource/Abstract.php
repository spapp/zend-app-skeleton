<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.26.
 */
/**
 * Class Application_Resource_Abstract
 */
abstract class Application_Resource_Abstract extends Zend_Application_Resource_ResourceAbstract {
    /**
     * @var mixed
     */
    protected $adapter = null;

    /**
     * Strategy pattern: initialize resource
     *
     * @see Zend_Application_Resource_Resource
     * @return mixed
     */
    public function init() {
        return $this->getAdapter();
    }

    /**
     * Sets the adapter options.
     *
     * @param array $params
     *
     * @return $this
     */
    public function setParams(array $params) {
        $this->getAdapter()->setOptions($params);

        return $this;
    }

    /**
     * Returns the adapter.
     *
     * @return Application_Script_Abstract
     */
    public function getAdapter() {
        return $this->adapter;
    }

    /**
     * Sets the adapter.
     *
     * @param string $adapterName
     *
     * @return $this
     */
    abstract public function setAdapter($adapterName);
}
