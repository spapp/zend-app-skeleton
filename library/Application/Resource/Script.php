<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.22.
 */
/**
 * Class Application_Resource_Script
 */
class Application_Resource_Script extends Application_Resource_Abstract {
    /**
     * Sets the script adapter.
     *
     * @param string $adapter
     *
     * @return $this
     */
    public function setAdapter($adapter) {
        $this->adapter = Application_Script::factory($adapter);

        return $this;
    }
}