<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.22.
 */
/**
 * Class Application_Controller_Plugin_Skin
 */
class Application_Controller_Plugin_Skin extends Zend_Controller_Plugin_Abstract {
    /**
     * Called after Zend_Controller_Router exits.
     *
     * Called after Zend_Controller_Front exits from the router.
     *
     * @see Zend_Controller_Plugin_Abstract::routeShutdown()
     *
     * @param  Zend_Controller_Request_Abstract $request
     *
     * @return void
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request) {
        $skin = $this->getResource('skin');

        if ($skin->getControllerName() === $request->getControllerName()) {
            $this->sendResponse();
            exit(0);
        } else {
            $this->appendFile($skin->getFiles());
            $this->appendFile($skin->getLinkHref());
        }
    }

    /**
     * Returns a resource.
     *
     * @param string $name
     *
     * @return null|mixed
     */
    protected function getResource($name) {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');

        if ($bootstrap->hasResource($name)) {
            return $bootstrap->getResource($name);
        }

        return null;
    }

    /**
     * Appends stylesheet link to html output.
     *
     * @param array|string $files
     *
     * @return $this
     */
    protected function appendFile($files) {
        $files = (array)$files;
        $headLink = $this->getResource('view')->headLink();

        foreach ($files as $file) {
            $headLink->appendStylesheet($file);
        }

        return $this;
    }

    /**
     * Sets response http headers.
     *
     * @return $this
     */
    protected function setHeaders() {
        $headers = $this->getResource('skin')->getHeaders();

        foreach ($headers as $name => $value) {
            $this->getResponse()->setHeader($name, $value);
        }

        return $this;
    }

    /**
     * Sends response.
     *
     * @return void
     */
    protected function sendResponse() {
        $skin = $this->getResource('skin');

        if ($this->getRequest()->getActionName() !== Zend_Controller_Front::getInstance()->getDefaultAction()) {
            $skin->setSkinName($this->getRequest()->getActionName());
        }

        $this->setHeaders();
        $this->getResponse()->setBody((string)$skin)->sendResponse();
    }
} 