<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.22.
 */
/**
 * Class Application_Controller_Plugin_Script
 */
class Application_Controller_Plugin_Script extends Zend_Controller_Plugin_Abstract {
    /**
     * Called after Zend_Controller_Router exits.
     *
     * Called after Zend_Controller_Front exits from the router.
     *
     * @see Zend_Controller_Plugin_Abstract::routeShutdown()
     *
     * @param  Zend_Controller_Request_Abstract $request
     *
     * @return void
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request) {
        $script = $this->getResource('script');

        if ($script->getControllerName() === $request->getControllerName()) {
            $this->sendResponse();
            exit(0);
        } else {
            $this->appendFile($script->getScripts());
        }
    }

    /**
     * Returns a resource.
     *
     * @param string $name
     *
     * @return null|mixed
     */
    protected function getResource($name) {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');

        if ($bootstrap->hasResource($name)) {
            return $bootstrap->getResource($name);
        }

        return null;
    }

    /**
     * Appends script tag to html output.
     *
     * @param array|string $files
     *
     * @return $this
     */
    protected function appendFile($files) {
        $files = (array)$files;
        $headScript = $this->getResource('view')->headScript();

        foreach ($files as $file) {
            $headScript->appendFile($file);
        }

        return $this;
    }

    /**
     * Sets response http headers.
     *
     * @return $this
     */
    protected function setHeaders() {
        $headers = $this->getResource('script')->getHeaders();

        foreach ($headers as $name => $value) {
            $this->getResponse()->setHeader($name, $value);
        }

        return $this;
    }

    /**
     * Returns javascript file name if it is exists.
     *
     * @return string|null
     */
    protected function getFileName() {
        $fileName = preg_replace('~^/' . $this->getRequest()->getControllerName() . '/~',
                                 '',
                                 $this->getRequest()->getServer('REQUEST_URI'));
        if (!preg_match('~\.js$~', $fileName)) {
            $fileName = null;
        }

        return $fileName;
    }

    /**
     * Sends response.
     *
     * @return void
     */
    protected function sendResponse() {
        $script = $this->getResource('script');

        $script->setScriptName($this->getFileName());

        $this->setHeaders();
        $this->getResponse()->setBody((string)$script)->sendResponse();
    }
}


