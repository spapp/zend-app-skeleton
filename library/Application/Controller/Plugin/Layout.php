<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.27.
 */
/**
 * Class Application_Controller_Plugin_Layout
 */
class Application_Controller_Plugin_Layout extends Zend_Layout_Controller_Plugin_Layout {
    /**
     * @var Zend_Loader_PluginLoader
     */
    protected $filterPluginLoader = null;

    /**
     * Layout options.
     *
     * @var array
     */
    protected $options = null;

    /**
     * Default filter path for plugin loader.
     *
     * @var array
     */
    protected $defaultFilterPath=array(
        'Application_View_Filter' => 'Application/View/Filter'
    );

    /**
     * Called before Zend_Controller_Front exits its dispatch loop.
     *
     * @see Zend_Controller_Plugin_Abstract::dispatchLoopShutdown()
     * @return void
     */
    public function dispatchLoopShutdown() {
        $body = $this->getResponse()->getBody();

        if ($this->getOption('filterPath')) {
            $this->setFilterPath($this->getOption('filterPath'));
        }

        if ($this->getOption('filter')) {
            $filters = $this->getOption('filter');

            foreach ((array)$filters as $filter => $enabled) {
                if (true === $enabled) {
                    $body = $this->filterOutput($body, $filter);
                }
            }
        }

        $this->getResponse()->setBody($body);
    }

    /**
     * Returns special option.
     *
     * @param null|string $name
     *
     * @return mixed|null
     */
    protected function getOption($name = null) {
        if (null === $this->options) {
            $options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

            if (array_key_exists('resources', $options) and array_key_exists('layout', $options['resources'])) {
                $this->options = $options['resources']['layout'];
            }
        }

        if (is_array($this->options) and array_key_exists($name, $this->options)) {
            return $this->options[$name];
        }

        return (null === $name) ? $this->options : null;
    }

    /**
     * Returns a plugin loader.
     *
     * @return Zend_Loader_PluginLoader
     */
    protected function getPluginLoader() {
        if (null === $this->filterPluginLoader) {
            $this->filterPluginLoader = new Zend_Loader_PluginLoader($this->defaultFilterPath);
        }

        return $this->filterPluginLoader;
    }

    /**
     * Sets some filter paths.
     *
     * @param array $paths
     *
     * @return $this
     */
    protected function setFilterPath(array $paths) {
        foreach ($paths as $classPrefix => $path) {
            $this->getPluginLoader()->addPrefixPath($classPrefix, $path);
        }

        return $this;
    }

    /**
     * Filtering output.
     *
     * @param string $output
     * @param string $filterPluginName
     *
     * @return string
     */
    protected function filterOutput($output, $filterPluginName) {
        $filterClass = $this->getPluginLoader()->load($filterPluginName);
        $filterClass = new $filterClass();

        if (method_exists($filterClass, 'setView')) {
            $filterClass->setView($this->getLayout()->getView());
        }

        return call_user_func(array(
                                  $filterClass,
                                  'filter'
                              ),
                              $output);
    }
} 