<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.21.
 */
/**
 * Class Application_Controller_Helper_Log
 */
class Application_Controller_Helper_Log extends Zend_Controller_Action_Helper_Abstract {
    protected $logger = null;

    /**
     * Perform helper when called as $this->_helper->log() from an action controller
     *
     * Proxies to {@link log()}
     *
     * @param  string  $message  Message to log
     * @param  integer $priority Priority of message
     * @param  mixed   $extras   Extra information to log in event
     *
     * @return void
     */
    public function direct($message, $priority = Zend_Log::INFO, $extras = null) {
        $this->log($message, $priority, $extras);
    }

    /**
     * Log a message at a priority
     *
     * @param  string  $message  Message to log
     * @param  integer $priority Priority of message
     * @param  mixed   $extras   Extra information to log in event
     *
     * @return void
     */
    public function log($message, $priority = Zend_Log::INFO, $extras = null) {
        $this->getLogger()->log($message, $priority, $extras);
    }

    /**
     * Returns the logger and one or more writers based on the resource configuration.
     *
     * @return Zend_Log
     * @throws Zend_Exception
     */
    protected function getLogger() {
        if (null === $this->logger) {
            $bootstrap = $this->getFrontController()->getParam('bootstrap');

            if (true !== $bootstrap->hasPluginResource('Log')) {
                throw new Zend_Exception('Log not enabled in the aplication config.');
            }

            $this->logger = $bootstrap->getResource('Log');
        }

        return $this->logger;
    }
} 