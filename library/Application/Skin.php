<?php
/**
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   zend_app_skeleton
 * @version   1.0.0
 * @since     2013.11.25.
 */
/**
 * Class Application_Skin
 */
class Application_Skin {
    /**
     * Constructor
     */
    protected function __construct() {

    }

    /**
     * Returns a skin adapter.
     *
     * @param string                 $adapterName
     * @param null|array|Zend_Config $options
     *
     * @return Application_Skin_Abstract
     */
    public static function factory($adapterName, $options = null) {
        $className = 'Application_Skin_' . ucfirst(strtolower($adapterName));

        return new $className($options);
    }
} 