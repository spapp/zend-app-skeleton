#!/bin/sh
# @author    Sandor Papp <spapp@spappsite.hu>
# @copyright 2013
# @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
# @package   zend_app_skeleton
# @version   1.0.0
# @since     2013.11.18.

SCRIPT="`readlink -f ${0}`"
SCRIPT_PATH="`dirname ${SCRIPT}`"

echo 'Path to the Zend Framework: ';

read path

cd "${SCRIPT_PATH}/../library"

ln -sv ${path}