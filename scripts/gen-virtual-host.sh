#!/bin/bash
# @author    Sandor Papp <spapp@spappsite.hu>
# @copyright 2013
# @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
# @package   zend_app_skeleton
# @version   1.0.0
# @since     2013.11.18.
#
# Helper script.
#
# Generates a typical apache virtual host config under http-virtual-host template.
#

SCRIPT="`readlink -f ${0}`"
SCRIPT_PATH="`dirname ${SCRIPT}`"
HTTP_VIRTUAL_HOST_TPL="${SCRIPT_PATH}/http-virtual-host"

DEFAULT_PORT=80
DEFAULT_DOMAIN_NAME='example.tld'
DEFAULT_PUBLIC_FOLDER="`dirname ${SCRIPT_PATH}`/public_html"
DEFAULT_LOG_FOLDER="/tmp"
DEFAULT_APPLICATION_ENV='development'

COLOR_NORMAL="\033[0;0m"
COLOR_BOLD="\033[1;37m"
COLOR_HOT="\033[1;31m"

function echo_q(){
    echo -ne "${COLOR_BOLD}${1} (${COLOR_NORMAL}${2}${COLOR_BOLD}):${COLOR_NORMAL}"
}

function echo_h(){
    echo -e "${COLOR_HOT}${1}${COLOR_NORMAL}"
}

echo_q "Port" ${DEFAULT_PORT}
read PORT

echo_q "Domain name" ${DEFAULT_DOMAIN_NAME}
read DOMAIN_NAME

echo_q "Path to public folder" ${DEFAULT_PUBLIC_FOLDER}
read PUBLIC_FOLDER

echo_q "Application enviroment" ${DEFAULT_APPLICATION_ENV}
read APPLICATION_ENV

echo_q "Path to log folder" ${DEFAULT_LOG_FOLDER}
read LOG_FOLDER


out=$(sed -e "s/{PORT}/${PORT:-${DEFAULT_PORT}}/g" < ${HTTP_VIRTUAL_HOST_TPL})
out=$(sed -e "s/{DOMAIN_NAME}/${DOMAIN_NAME:-${DEFAULT_DOMAIN_NAME}}/g" <<<"${out}")
out=$(sed -e "s;{PUBLIC_FOLDER};${PUBLIC_FOLDER:-${DEFAULT_PUBLIC_FOLDER}};g" <<<"${out}")
out=$(sed -e "s;{LOG_FOLDER};${LOG_FOLDER:-${DEFAULT_LOG_FOLDER}};g" <<<"${out}")
out=$(sed -e "s/{APPLICATION_ENV}/${APPLICATION_ENV:-${DEFAULT_APPLICATION_ENV}}/g" <<<"${out}")

echo_h "Httpd Virtual Host:"
echo -e "\n${out}\n\n"

echo_h "Do not forget:\n"
echo "# /etc/hosts"
echo "127.0.0.1       ${DOMAIN_NAME:-${DEFAULT_DOMAIN_NAME}}"
echo
